#include <iostream>
#include <string>

#include <boost/shared_ptr.hpp>

#include <arc/arc.h>

using namespace std;
using boost::shared_ptr;

class TextEntity : public arc::Entity
{
public:
    string value;

    TextEntity(const string& value = "") : value(value) {};
};

class TextSystem : public arc::System<TextEntity>
{
    shared_ptr<arc::I18nAssetEntity> i18n;

public:
    static const string key;

    TextSystem(shared_ptr<arc::I18nAssetEntity> i18n) : i18n(i18n) {}
    void update()
    {
        cout << "<- " + i18n->get("updating") + "..." << endl;

        for(const_iterator
                it = entities.begin(),
                ite = entities.end();
            it != ite; ++it)
        {
            cout << (*it)->value << endl;
        }

        cout << "-> " + i18n->get("updated") + "!" << endl << endl;
    }
};
const string TextSystem::key = "text";

class TextFactory : public arc::EntityFactory<TextEntity, TextSystem>
{
public:
    TextFactory(TextSystem& system) : EntityFactory(system) {}

    shared_ptr<TextEntity> createText(const string& text)
    {
        shared_ptr<TextEntity> entity(new TextEntity(text));
        addEntity(entity);
        return entity;
    }
};

int main(int argc, char* argv[])
{
    arc::SystemHost host;

    arc::AssetSystem assets;
    host.addSystem(arc::AssetSystem::key, assets);
    arc::AssetFactory assetFactory(assets);
    shared_ptr<arc::I18nAssetEntity> i18n(assetFactory.getI18n(
        arc::utils::relPath(argv[0], "../test/assets/i18n"), "gb"));

    TextSystem text(i18n);
    host.addSystem(TextSystem::key, text);
    TextFactory textFactory(text);

    host.start();

    textFactory.createText(i18n->get("one"));
    host.update();

    shared_ptr<TextEntity> bar(textFactory.createText(i18n->get("two")));
    host.update();

    textFactory.createText(i18n->get("three"));
    host.update();

    text.removeEntity(bar);
    host.update();

    cout
        << "Group creation: "
        << host.createGroup() << ", "
        << host.createGroup() << ", "
        << host.createGroup() << endl;

    host.stop();

    return 0;
}