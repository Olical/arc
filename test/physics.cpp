#include <fstream>
#include <math.h>

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/shared_ptr.hpp>

#include <arc/arc.h>

using boost::property_tree::ptree;
using boost::shared_ptr;

class RectanglePhysics : public arc::GraphicPhysicsEntity
{
public:
    int ppm;
    b2Body* body;

    void update()
    {
        b2Vec2 pos = body->GetPosition();
        sf::Transformable* transformable = &graphics->getTransformable();
        transformable->setPosition(ppm * pos.x, ppm * pos.y);
        // transformable->setRotation(body->GetAngle() * 180 / M_PI);
    }
};

shared_ptr<RectanglePhysics> makeRecPhys(
    arc::PhysicsSystem& physics,
    shared_ptr<arc::GraphicEntity> rec,
    const b2Vec2& position,
    bool fixed = false,
    bool fixedRotation = false)
{
    b2BodyDef bodyDef;
    b2PolygonShape box;
    b2FixtureDef fixtureDef;
    shared_ptr<RectanglePhysics> phys(new RectanglePhysics);

    phys->graphics = rec;

    if(!fixed)
    {
        bodyDef.type = b2_dynamicBody;
    }

    bodyDef.position.Set(position.x / physics.ppm, position.y / physics.ppm);
    phys->body = physics.world.CreateBody(&bodyDef);

    sf::Vector2f size = sf::Vector2f(rec->getSize());
    rec->getTransformable().setOrigin(size.x / 2.0f, size.y / 2.0f);
    box.SetAsBox(size.x / physics.ppm / 2.0f, size.y / physics.ppm / 2.0f);

    fixtureDef.shape = &box;

    if(fixed)
    {
        fixtureDef.density = 0.0f;
    }
    else
    {
        fixtureDef.density = 1.0f;
    }

    // fixtureDef.friction = 1;
    phys->body->CreateFixture(&fixtureDef);
    phys->body->SetFixedRotation(fixedRotation);

    phys->ppm = physics.ppm;
    physics.addEntity(phys);
    return phys;
}

class MovementKeys : public arc::KeyEntity
{
    shared_ptr<RectanglePhysics> target;
    float accel;

public:
    MovementKeys(shared_ptr<RectanglePhysics> target, float accel = 20.0f) :
        target(target), accel(accel) {}
    void handle()
    {
        b2Vec2 force = b2Vec2(0.0f, 0.0f);
        if(isPressed(sf::Keyboard::Up))
        {
            force.y -= accel;
        }
        if(isPressed(sf::Keyboard::Right))
        {
            force.x += accel;
        }
        if(isPressed(sf::Keyboard::Down))
        {
            force.y += accel;
        }
        if(isPressed(sf::Keyboard::Left))
        {
            force.x -= accel;
        }
        target->body->ApplyForceToCenter(force);
    }
};

class RectanglePersistence : public arc::PersistenceEntity
{
    shared_ptr<RectanglePhysics> rectangle;

public:
    RectanglePersistence(
        shared_ptr<RectanglePhysics> rectangle) : rectangle(rectangle) {}

    void load(const ptree& pt)
    {
        b2Vec2 position;
        position.x = pt.get<float>("physics.position.<xmlattr>.x");
        position.y = pt.get<float>("physics.position.<xmlattr>.y");
        rectangle->body->SetTransform(
            position, rectangle->body->GetAngle());
    }

    void save(ptree& pt)
    {
        b2Vec2 position = rectangle->body->GetPosition();
        pt.put("physics.position.<xmlattr>.x", position.x);
        pt.put("physics.position.<xmlattr>.y", position.y);
    }
};

int main(int argc, char* argv[])
{
    arc::SystemHost host;

    arc::PersistenceSystem persistence(
        arc::utils::relPath(argv[0], "physics.xml"));
    host.addSystem(arc::PersistenceSystem::key, persistence);

    arc::EventSystem events;
    host.addSystem(arc::EventSystem::key, events);

    arc::KeySystem keys;
    host.addSystem(arc::KeySystem::key, keys);

    arc::PhysicsSystem physics(b2Vec2(0.0f, 0.0f));
    host.addSystem(arc::PhysicsSystem::key, physics);

    arc::GraphicSystem graphics(
        sf::VideoMode(800, 600), "Basic window", sf::Color::White);
    graphics.window.setVerticalSyncEnabled(true);
    host.addSystem(arc::GraphicSystem::key, graphics);

    arc::AssetSystem assets;
    arc::AssetFactory assetFactory(assets);
    host.addSystem(arc::AssetSystem::key, assets);

    shared_ptr<arc::ImageGraphicEntity> rec1(
        new arc::ImageGraphicEntity(assetFactory.getImage(
            arc::utils::relPath(argv[0], "../test/assets/small.png"))));
    graphics.addEntity(rec1);

    shared_ptr<arc::ImageGraphicEntity> rec2(
        new arc::ImageGraphicEntity(assetFactory.getImage(
            arc::utils::relPath(argv[0], "../test/assets/large.png"))));
    graphics.addEntity(rec2);

    shared_ptr<arc::SpriteGraphicEntity> guy(
        new arc::SpriteGraphicEntity(
            assetFactory.getImage(
                arc::utils::relPath(argv[0], "../test/assets/spinner.png"))));
    guy->setCellSize(sf::Vector2f(26, 26));
    graphics.addEntity(guy);

    arc::AnimationSystem animations;
    host.addSystem(arc::AnimationSystem::key, animations);
    shared_ptr<arc::AnimationEntity> anim(new arc::AnimationEntity);
    anim->setLoop(true);
    anim->start(0, 11, 1000);
    animations.addEntity(anim);

    shared_ptr<RectanglePhysics> rec1Phys(makeRecPhys(
        physics, rec1, b2Vec2(80.0f, 80.0f), false, true));
    makeRecPhys(
        physics, rec2, b2Vec2(310.0f, 285.0f), true);

    shared_ptr<RectanglePersistence> rec1Save(new RectanglePersistence(rec1Phys));
    persistence.addEntity(rec1Save);

    shared_ptr<MovementKeys> movementKeys(new MovementKeys(rec1Phys));
    keys.addEntity(movementKeys);

    host.start();
    while(graphics.window.isOpen())
    {
        guy->setCell(sf::Vector2i(anim->getValue(), 0));
        host.update();
    }
    host.stop();

    return 0;
}