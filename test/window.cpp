#include <SFML/Graphics.hpp>
#include <boost/shared_ptr.hpp>

#include <arc/arc.h>

using boost::shared_ptr;

class RectangleEntity : public arc::GraphicEntity
{
public:
    sf::RectangleShape rectangle;

    RectangleEntity(
        const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Color& color)
    {
        rectangle.setSize(size);
        rectangle.setPosition(position);
        rectangle.setFillColor(color);
    }

    sf::Drawable& getDrawable()
    {
        return rectangle;
    }

    sf::Transformable& getTransformable()
    {
        return rectangle;
    }

    sf::Vector2i getSize()
    {
        return sf::Vector2i(rectangle.getSize());
    }

    void applyFocus()
    {
        rectangle.rotate(45);
    }

    void removeFocus()
    {
        rectangle.rotate(-45);
    }
};

class ClickFocusEventEntity : public arc::EventEntity
{
    arc::GraphicSystem* graphics;

public:
    ClickFocusEventEntity(arc::GraphicSystem& gs) : graphics(&gs) {}

    void handle(const sf::Event& evt)
    {
        if(
            evt.type == sf::Event::MouseButtonPressed &&
            evt.mouseButton.button == sf::Mouse::Left)
        {
            graphics->setFocussed(evt.mouseButton.x, evt.mouseButton.y);
        }
        else if(evt.type == sf::Event::KeyPressed)
        {
            switch(evt.key.code)
            {
                case sf::Keyboard::Up:
                    graphics->setFocussed(0.0f);
                    break;
                case sf::Keyboard::Right:
                    graphics->setFocussed(90.0f);
                    break;
                case sf::Keyboard::Down:
                    graphics->setFocussed(180.0f);
                    break;
                case sf::Keyboard::Left:
                    graphics->setFocussed(270.0f);
                    break;
                default: break;
            }
        }
    }
};

int main(int argc, char* argv[])
{
    arc::SystemHost host;

    arc::EventSystem events;
    host.addSystem(arc::EventSystem::key, events);

    arc::GraphicSystem graphics(
        sf::VideoMode(800, 600), "Basic window", sf::Color::White);
    arc::GraphicFactory graphicFactory(graphics);
    host.addSystem(arc::GraphicSystem::key, graphics);

    shared_ptr<ClickFocusEventEntity> cfee(new ClickFocusEventEntity(graphics));
    events.addEntity(cfee);

    shared_ptr<RectangleEntity>
        a(new RectangleEntity(
            sf::Vector2f(60.0f, 60.0f),
            sf::Vector2f(100.0f, 100.0f),
            sf::Color(255, 0, 0))),
        b(new RectangleEntity(
            sf::Vector2f(80.0f, 80.0f),
            sf::Vector2f(300.0f, 180.0f),
            sf::Color(0, 255, 0))),
        c(new RectangleEntity(
            sf::Vector2f(100.0f, 400.0f),
            sf::Vector2f(5.0f, 5.0f),
            sf::Color(200, 200, 200, 200))),
        d(new RectangleEntity(
            sf::Vector2f(80.0f, 80.0f),
            sf::Vector2f(300.0f, 180.0f),
            sf::Color(100, 200, 100)));

    b->setRight(10);
    b->setBottom(10);

    d->setRight(100);
    d->setBottom(10);

    a->order = 10;
    c->order = -10;

    graphics.addEntity(a);
    graphics.addEntity(b);
    graphics.addEntity(c);
    graphics.addEntity(d);

    arc::AssetSystem assets;
    host.addSystem(arc::AssetSystem::key, assets);
    arc::AssetFactory assetFactory(assets);
    shared_ptr<arc::SoundAssetEntity> gangnam = assetFactory.getSound(
        arc::utils::relPath(argv[0], "../test/assets/gangnam.ogg"));
    gangnam->sound.setVolume(0.1f);
    gangnam->sound.play();

    arc::AnimationSystem animations;
    host.addSystem(arc::AnimationSystem::key, animations);

    shared_ptr<arc::AnimationEntity> anim(new arc::AnimationEntity);
    animations.addEntity(anim);
    int animLength = 4000;
    anim->start(90.0f, 400.0f, animLength);
    bool reverse = false;

    shared_ptr<arc::TextGraphicEntity>exText(graphicFactory.createText(
        "Lorem ipsum dolor sit amet.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in lorem massa, a commodo risus.",
        assetFactory.getFont(
            arc::utils::relPath(argv[0], "../test/assets/DroidSans.ttf")),
        20));
    exText->getTransformable().setPosition(sf::Vector2f(5, 5));
    exText->setWidth(100);

    a->focusable = true;
    b->focusable = true;
    c->focusable = true;
    d->focusable = true;

    host.start();
    while(graphics.window.isOpen())
    {
        a->setLeft(anim->getValue());
        //exText->setWidth(anim->getValue());
        //c->rectangle.setSize(sf::Vector2f(anim->getValue(), 400));
        if(!anim->isRunning())
        {
            if(reverse)
            {
                reverse = false;
                anim->start(90.0f, 400.0f, animLength);
            }
            else
            {
                reverse = true;
                anim->start(400.0f, 90.0f, animLength);
            }
        }

        host.update();
    }
    host.stop();

    return 0;
}