#include <arc/SoundAssetEntity.h>

void arc::SoundAssetEntity::load()
{
    buffer.loadFromFile(file);
    sound.setBuffer(buffer);
}