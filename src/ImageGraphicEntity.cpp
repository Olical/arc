#include <arc/ImageGraphicEntity.h>

sf::Drawable& arc::ImageGraphicEntity::getDrawable()
{
    return sprite;
}

sf::Transformable& arc::ImageGraphicEntity::getTransformable()
{
    return sprite;
}

sf::Vector2i arc::ImageGraphicEntity::getSize()
{
    return size;
}