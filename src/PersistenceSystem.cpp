#include <arc/PersistenceSystem.h>

const string arc::PersistenceSystem::key = "persistence";

void arc::PersistenceSystem::start()
{
    if(fs::exists(file))
    {
        ptree pt;
        read_xml(file, pt);

        for(const_iterator
                it = entities.begin(),
                ite = entities.end();
            it != ite; ++it)
        {
            (*it)->load(pt);
        }
    }
}

void arc::PersistenceSystem::stop()
{
    ptree pt;

    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        (*it)->save(pt);
    }

    write_xml(file, pt);
}