#include <arc/easing.h>

/*
    t: current time
    b: start value
    c: change in value
    d: duration
    From http://robertpenner.com/easing/
*/

// Linear
float arc::easing::linear(float t, float b, float c, float d)
{
    return c*t/d + b;
}

// Back
float arc::easing::back::in(float t, float b, float c, float d)
{
    return c*(t/=d)*t*((s+1.0f)*t - s) + b;
}

float arc::easing::back::out(float t, float b, float c, float d)
{
    return c*((t=t/d-1.0f)*t*((s+1.0f)*t + s) + 1.0f) + b;
}

float arc::easing::back::inOut(float t, float b, float c, float d)
{
    float s = back::s;
    if ((t/=d/2.0f) < 1.0f) return c/2.0f*(t*t*(((s*=(1.525))+1.0f)*t - s)) + b;
    return c/2.0f*((t-=2.0f)*t*(((s*=(1.525))+1.0f)*t + s) + 2.0f) + b;
}

// Bounce
float arc::easing::bounce::in(float t, float b, float c, float d)
{
    
    return c - bounce::out(d-t, 0.0f, c, d) + b;
}

float arc::easing::bounce::out(float t, float b, float c, float d)
{
    if ((t/=d) < (1/2.75f)) {
        return c*(7.5625f*t*t) + b;
    } else if (t < (2.0f/2.75f)) {
        return c*(7.5625f*(t-=(1.5f/2.75f))*t + .75f) + b;
    } else if (t < (2.5f/2.75f)) {
        return c*(7.5625f*(t-=(2.25f/2.75f))*t + .9375f) + b;
    } else {
        return c*(7.5625f*(t-=(2.625f/2.75f))*t + .984375f) + b;
    }
}

float arc::easing::bounce::inOut(float t, float b, float c, float d)
{
    if (t < d/2.0f) return bounce::in(t*2.0f, 0.0f, c, d) * 0.5f + b;
    else return bounce::out(t*2.0f-d, 0.0f, c, d) * 0.5f + c*0.5f + b;
}

// Circ
float arc::easing::circ::in(float t, float b, float c, float d)
{
    return -c * (sqrt(1.0f - (t/=d)*t) - 1.0f) + b;
}

float arc::easing::circ::out(float t, float b, float c, float d)
{
    return c * sqrt(1.0f - (t=t/d-1.0f)*t) + b;
}

float arc::easing::circ::inOut(float t, float b, float c, float d)
{
    if ((t/=d/2.0f) < 1.0f) return -c/2.0f * (sqrt(1.0f - t*t) - 1.0f) + b;
    return c/2.0f * (sqrt(1.0f - (t-=2.0f)*t) + 1.0f) + b;
}

// Cubic
float arc::easing::cubic::in(float t, float b, float c, float d)
{
    return c*(t/=d)*t*t + b;
}

float arc::easing::cubic::out(float t, float b, float c, float d)
{
    return c*((t=t/d-1.0f)*t*t + 1.0f) + b;
}

float arc::easing::cubic::inOut(float t, float b, float c, float d)
{
    if ((t/=d/2.0f) < 1.0f) return c/2.0f*t*t*t + b;
    return c/2.0f*((t-=2.0f)*t*t + 2.0f) + b;
}

// Elastic
float arc::easing::elastic::in(float t, float b, float c, float d)
{
    float a = elastic::a, p = elastic::p, s;
    if (t==0.0f) return b;  if ((t/=d)==1.0f) return b+c;  if (!p) p=d*0.3f;
    if (!a || a < fabs(c)) { a=c; s=p/4.0f; }
    else s = p/(2.0f*M_PI) * asin (c/a);
    return -(a*pow(2.0f,10.0f*(t-=1.0f)) * sin( (t*d-s)*(2.0f*M_PI)/p )) + b;
}

float arc::easing::elastic::out(float t, float b, float c, float d)
{
    float a = elastic::a, p = elastic::p, s;
    if (t==0.0f) return b;  if ((t/=d)==1.0f) return b+c;  if (!p) p=d*0.3f;
    if (!a || a < fabs(c)) { a=c; s=p/4.0f; }
    else s = p/(2.0f*M_PI) * asin (c/a);
    return (a*pow(2.0f,-10.0f*t) * sin( (t*d-s)*(2.0f*M_PI)/p ) + c + b);
}

float arc::easing::elastic::inOut(float t, float b, float c, float d)
{
    float a = elastic::a, p = elastic::p, s;
    if (t==0.0f) return b;  if ((t/=d/2.0f)==2.0f) return b+c;  if (!p) p=d*(0.3f*1.5f);
    if (!a || a < fabs(c)) { a=c; s=p/4.0f; }
    else s = p/(2.0f*M_PI) * asin (c/a);
    if (t < 1.0f) return -0.5f*(a*pow(2.0f,10.0f*(t-=1.0f)) * sin( (t*d-s)*(2.0f*M_PI)/p )) + b;
    return a*pow(2.0f,-10.0f*(t-=1.0f)) * sin( (t*d-s)*(2.0f*M_PI)/p )*0.5f + c + b;
}

// Expo
float arc::easing::expo::in(float t, float b, float c, float d)
{
    return (t==0.0f) ? b : c * pow(2.0f, 10.0f * (t/d - 1.0f)) + b;
}

float arc::easing::expo::out(float t, float b, float c, float d)
{
    return (t==d) ? b+c : c * (-pow(2.0f, -10.0f * t/d) + 1.0f) + b;
}

float arc::easing::expo::inOut(float t, float b, float c, float d)
{
    if (t==0.0f) return b;
    if (t==d) return b+c;
    if ((t/=d/2.0f) < 1.0f) return c/2.0f * pow(2.0f, 10.0f * (t - 1.0f)) + b;
    return c/2.0f * (-pow(2.0f, -10.0f * --t) + 2.0f) + b;
}

// Quad
float arc::easing::quad::in(float t, float b, float c, float d)
{
    return c*(t/=d)*t + b;
}

float arc::easing::quad::out(float t, float b, float c, float d)
{
    return -c *(t/=d)*(t-2.0f) + b;
}

float arc::easing::quad::inOut(float t, float b, float c, float d)
{
    if ((t/=d/2.0f) < 1.0f) return c/2.0f*t*t + b;
    return -c/2.0f * ((--t)*(t-2.0f) - 1.0f) + b;
}

// Quart
float arc::easing::quart::in(float t, float b, float c, float d)
{
    return c*(t/=d)*t*t*t + b;
}

float arc::easing::quart::out(float t, float b, float c, float d)
{
    return -c * ((t=t/d-1.0f)*t*t*t - 1.0f) + b;
}

float arc::easing::quart::inOut(float t, float b, float c, float d)
{
    if ((t/=d/2.0f) < 1.0f) return c/2.0f*t*t*t*t + b;
    return -c/2.0f * ((t-=2.0f)*t*t*t - 2.0f) + b;
}

// Quint
float arc::easing::quint::in(float t, float b, float c, float d)
{
    return c*(t/=d)*t*t*t*t + b;
}

float arc::easing::quint::out(float t, float b, float c, float d)
{
    return c*((t=t/d-1.0f)*t*t*t*t + 1.0f) + b;
}

float arc::easing::quint::inOut(float t, float b, float c, float d)
{
    if ((t/=d/2.0f) < 1.0f) return c/2.0f*t*t*t*t*t + b;
    return c/2.0f*((t-=2.0f)*t*t*t*t + 2.0f) + b;
}

// Sine
float arc::easing::sine::in(float t, float b, float c, float d)
{
    return -c * cos(t/d * (M_PI/2.0f)) + c + b;
}

float arc::easing::sine::out(float t, float b, float c, float d)
{
    return c * sin(t/d * (M_PI/2.0f)) + b;
}

float arc::easing::sine::inOut(float t, float b, float c, float d)
{
    return -c/2.0f * (cos(M_PI*t/d) - 1.0f) + b;
}