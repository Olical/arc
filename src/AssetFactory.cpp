#include <arc/AssetFactory.h>

shared_ptr<arc::ImageAssetEntity> arc::AssetFactory::getImage(const string& file)
{
    return get<ImageAssetEntity>(file);
}

shared_ptr<arc::SoundAssetEntity> arc::AssetFactory::getSound(const string& file)
{
    return get<SoundAssetEntity>(file);
}

shared_ptr<arc::MusicAssetEntity> arc::AssetFactory::getMusic(const string& file)
{
    return get<MusicAssetEntity>(file);
}

shared_ptr<arc::XMLAssetEntity> arc::AssetFactory::getXML(const string& file)
{
    return get<XMLAssetEntity>(file);
}

shared_ptr<arc::I18nAssetEntity> arc::AssetFactory::getI18n(
    const string& rawFile, const string& region)
{
    return get<I18nAssetEntity>(I18nAssetEntity::buildPath(rawFile, region));
}

shared_ptr<arc::FontAssetEntity> arc::AssetFactory::getFont(const string& file)
{
    return get<FontAssetEntity>(file);
}