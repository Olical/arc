#include <arc/TextGraphicEntity.h>

sf::Drawable& arc::TextGraphicEntity::getDrawable()
{
    return text;
}

sf::Transformable& arc::TextGraphicEntity::getTransformable()
{
    return text;
}

sf::Vector2i arc::TextGraphicEntity::getSize()
{
    sf::FloatRect bounds(text.getLocalBounds());
    return sf::Vector2i(bounds.width, bounds.height);
}

void arc::TextGraphicEntity::setText(const string& content)
{
    text.setString(content);
    rawText = content;

    if(width != -1)
    {
        insertLineBreaks();
    }
}

string arc::TextGraphicEntity::getText()
{
    return rawText;
}

void arc::TextGraphicEntity::setFont(shared_ptr<FontAssetEntity> font)
{
    text.setFont(font->font);
}

void arc::TextGraphicEntity::setColor(const sf::Color& color)
{
    text.setColor(color);
}

void arc::TextGraphicEntity::setStyle(int style)
{
    text.setStyle(style);
}

void arc::TextGraphicEntity::setSize(int size)
{
    text.setCharacterSize(size);
}

sf::Vector2f arc::TextGraphicEntity::getCharacterPosition(int index)
{
    sf::Vector2f globalPos(text.findCharacterPos(index));
    return text.getInverseTransform().transformPoint(globalPos);
}

void arc::TextGraphicEntity::insertLineBreaks()
{
    string split = rawText;
    size_t index = 0, lastSpace = -1, lastHyphen = -1;

    // Measures the hyphen width then resets the text to the raw string.
    // The hyphen width is used to find the last viable hyphen position.
    text.setString("--");
    float hyphenWidth = getCharacterPosition(1).x;
    text.setString(rawText);

    for(string::iterator it = split.begin(); it != split.end(); ++it, index++)
    {
        if(*it == ' ')
        {
            lastSpace = index;
        }
        else if(getCharacterPosition(index).x + hyphenWidth <= width)
        {
            lastHyphen = index;
        }

        if(*it == '\n')
        {
            lastSpace = lastHyphen = -1;
        }
        else if(getCharacterPosition(index + 1).x >= width)
        {
            if(lastSpace != -1)
            {
                split[lastSpace] = '\n';
            }
            else
            {
                it = split.insert(split.begin() + lastHyphen, '\n');
                it = split.insert(split.begin() + lastHyphen, '-');
            }

            lastSpace = lastHyphen = -1;
            text.setString(split);
        }
    }
}

void arc::TextGraphicEntity::setWidth(int desWidth)
{
    width = desWidth;
    insertLineBreaks();
}