#include <arc/AnimationSystem.h>

const string arc::AnimationSystem::key = "animation";

void arc::AnimationSystem::update()
{
    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        (*it)->update();
    }
}