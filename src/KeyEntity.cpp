#include <arc/KeyEntity.h>

bool arc::KeyEntity::isPressed(const sf::Keyboard::Key& key)
{
    return sf::Keyboard::isKeyPressed(key);
}