#include <arc/I18nAssetEntity.h>

string arc::I18nAssetEntity::buildPath(const string& file, const string& region)
{
    return file + "." + region + ".xml";
}

string arc::I18nAssetEntity::get(const string& id, const string& root)
{
    ptree i18n(tree.get_child(root));
    for(ptree::const_iterator
            it = i18n.begin(),
            ite = i18n.end();
        it != ite; ++it)
    {
        if(it->second.get<string>("<xmlattr>.id") == id)
        {
            return it->second.data();
        }
    }

    return "I18n: \"" + id + "\" is undefined.";
}