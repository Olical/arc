#include <arc/AnimationEntity.h>

void arc::AnimationEntity::update()
{
    if(timer.isRunning())
    {
        int currTime = timer.getTime();

        if(currTime != lastCalc)
        {
            lastCalc = currTime;

            if(currTime > duration)
            {
                value = to;

                if(loop)
                {
                    timer.start();
                }
                else
                {
                    stop();
                }
            }
            else
            {
                value = easing(currTime, from, change, duration);
            }
        }
    }
}

float arc::AnimationEntity::getValue()
{
    return value;
}

void arc::AnimationEntity::start(float from, float to, int duration)
{
    this->from = from;
    this->to = to;
    this->change = to - from;
    this->duration = duration;
    timer.start();
}

void arc::AnimationEntity::stop()
{
    timer.stop();
}

bool arc::AnimationEntity::isRunning()
{
    return timer.isRunning();
}

void arc::AnimationEntity::setLoop(bool shouldLoop)
{
    loop = shouldLoop;
}