#include <arc/SpriteGraphicEntity.h>

void arc::SpriteGraphicEntity::setCellSize(const sf::Vector2f& size)
{
    cellSize = size;
}

void arc::SpriteGraphicEntity::setCell(const sf::Vector2i& cell)
{
    sprite.setTextureRect(sf::IntRect(
        cellSize.x * cell.x,
        cellSize.y * cell.y,
        cellSize.x,
        cellSize.y));
}