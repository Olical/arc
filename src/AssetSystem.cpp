#include <arc/AssetSystem.h>

const string arc::AssetSystem::key = "asset";

bool arc::AssetSystem::find(const string& file)
{
    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        if((*it)->file == file)
        {
            return true;
        }
    }

    return false;
}

shared_ptr<arc::AssetEntity> arc::AssetSystem::get(const string& file)
{
    shared_ptr<arc::AssetEntity> result;
    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        if((*it)->file == file)
        {
            result = *it;
        }
    }
    return result;
}