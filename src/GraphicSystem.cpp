#include <arc/GraphicSystem.h>

const string arc::GraphicSystem::key = "graphic";

void arc::GraphicSystem::start()
{
    window.create(videoMode, title, style, settings);
    clearWindow();
}

void arc::GraphicSystem::update()
{
    sf::Vector2u size = window.getSize();
    sf::Vector2i entitySize;
    sf::Vector2f entityPos;
    sf::Transformable* entity;
    bool usingRight, usingBottom, winSizeChanged = size != prevSize;
    clearWindow();

    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        usingRight = (*it)->isUsingRight();
        usingBottom = (*it)->isUsingBottom();
        if(usingRight || usingBottom)
        {
            entitySize = (*it)->getSize();
            if((*it)->hasSizeChanged(entitySize) || winSizeChanged)
            {
                entity = &(*it)->getTransformable();
                entityPos = entity->getPosition();

                if(usingRight)
                {
                    entityPos.x = size.x - entitySize.x - (*it)->getRight();
                }

                if(usingBottom)
                {
                    entityPos.y = size.y - entitySize.y - (*it)->getBottom();
                }

                entity->setPosition(entityPos);
            }
        }

        window.draw((*it)->getDrawable());
    }

    window.display();
    prevSize = size;
    fps = limit - floor(clock.restart().asMilliseconds() / 1000);
}

void arc::GraphicSystem::stop()
{
    if(window.isOpen())
    {
        window.close();
    }
}

void arc::GraphicSystem::clearWindow()
{
    window.clear(clearColor);
}

void arc::GraphicSystem::setFramerateLimit(int limit)
{
    this->limit = limit;
    window.setFramerateLimit(limit);
}

shared_ptr<arc::GraphicEntity> arc::GraphicSystem::entityAtPoint(
    int x, int y, bool requireFocus, int group)
{
    shared_ptr<GraphicEntity> found = noFocus;

    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        if(!requireFocus || ((*it)->focusable == true && (*it)->focusGroup == group))
        {
            if((*it)->containsPoint(x, y))
            {
                found = *it;
            }
        }
    }

    return found;
}

shared_ptr<arc::GraphicEntity> arc::GraphicSystem::getFocussed()
{
    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        if((*it)->focussed == true)
        {
            return *it;
        }
    }

    return noFocus;
}

void arc::GraphicSystem::setFocussed(shared_ptr<GraphicEntity> next)
{
    shared_ptr<GraphicEntity> orig(getFocussed());

    if(next->focusable == true && next != orig)
    {
        if(orig != noFocus)
        {
            orig->removeFocus();
            orig->focussed = false;
        }

        next->focussed = true;
        next->applyFocus();
    }
}

void arc::GraphicSystem::setFocussed(int x, int y, int group)
{
    shared_ptr<GraphicEntity> found = entityAtPoint(x, y, true, group);

    if(found != noFocus)
    {
        setFocussed(found);
    }
}

float arc::GraphicSystem::getBearingDistance(
    const sf::Vector2f& a, const sf::Vector2f& b, float cmp)
{
    float degs = atan2f(b.y - a.y, b.x - a.x) * (180 / M_PI) + 90;
    return 180.0f - fabs(fmod(fabs(degs - cmp), 360.0f) - 180.0f);
}

float arc::GraphicSystem::getDistance(
    const sf::Vector2f& a, const sf::Vector2f& b)
{
    float dx = b.x - a.x, dy = b.y - a.y;
    return sqrtf(powf(dx, 2.0f) + powf(dy, 2.0f));
}

void arc::GraphicSystem::setFocussed(float angle, int group)
{
    sf::Vector2f orig, curr;
    shared_ptr<GraphicEntity> focussed = getFocussed(), best = noFocus;
    float currAngle, currDistance, bestAngle, bestDistance;

    // Finds center position of focussed entity or center of window.
    if(focussed != noFocus)
    {
        orig = focussed->getTransformable().getPosition() +
            ((sf::Vector2f)focussed->getSize() / 2.0f);
    }
    else
    {
        orig = (sf::Vector2f)(window.getSize() / (unsigned int)2);
    }

    // Find best match for angle from original position.
    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        if(*it != focussed && (*it)->focusable && (*it)->focusGroup == group)
        {
            curr = (*it)->getTransformable().getPosition() +
                ((sf::Vector2f)(*it)->getSize() / 2.0f);

            currAngle = getBearingDistance(orig, curr, angle);
            currDistance = getDistance(orig, curr);

            if(currAngle < 90 && (
                currAngle < bestAngle ||
                currDistance < bestDistance ||
                best == noFocus))
            {
                best = *it;
                bestAngle = currAngle;
                bestDistance = currDistance;
            }
        }
    }

    if(best != noFocus)
    {
        setFocussed(best);
    }
}