#include <arc/KeySystem.h>

const string arc::KeySystem::key = "key";

void arc::KeySystem::update()
{
    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        (*it)->handle();
    }
}