#include <arc/EventSystem.h>

const string arc::EventSystem::key = "event";

void arc::EventSystem::start()
{
    if(graphics == 0)
    {
        graphics = (GraphicSystem*)host->getSystem(GraphicSystem::key);
    }
}

void arc::EventSystem::update()
{
    sf::Event event;
    while(graphics->window.pollEvent(event))
    {
        if(autoClose && event.type == sf::Event::Closed)
        {
            graphics->window.close();
        }
        else if(event.type == sf::Event::Resized)
        {
            graphics->window.setView(sf::View(
                sf::FloatRect(0, 0, event.size.width, event.size.height)));
        }

        for(const_iterator
                it = entities.begin(),
                ite = entities.end();
            it != ite; ++it)
        {
            (*it)->handle(event);
        }
    }
}