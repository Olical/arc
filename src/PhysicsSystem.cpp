#include <arc/PhysicsSystem.h>

const string arc::PhysicsSystem::key = "physics";

void arc::PhysicsSystem::update()
{
    world.Step(timeStep, velocityIterations, positionIterations);

    for(const_iterator
            it = entities.begin(),
            ite = entities.end();
        it != ite; ++it)
    {
        (*it)->update();
    }
}