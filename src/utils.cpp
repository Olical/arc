#include <arc/utils.h>

string arc::utils::relPath(const string& prog, const string& rawPath)
{
    fs::path
        progDir(fs::system_complete(prog).parent_path()),
        path(rawPath);

    return (progDir/path).string();
}

sf::Vector3f arc::utils::relSound(
    const sf::Vector2f& pos, const sf::Vector2u& size)
{
    sf::Vector3f res;
    res.x = pos.x - size.x / 2;
    res.y = pos.y - size.y / 2;
    return res;
}