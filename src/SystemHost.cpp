#include <arc/SystemHost.h>

void arc::SystemHost::addSystem(const string& key, Hostable& system, int group)
{
    systems.insert(SystemPair(key, &system));
    system.host = this;
    system.group = group;
}

void arc::SystemHost::removeSystem(const string& key)
{
    systems.erase(key);
}

arc::Hostable* arc::SystemHost::getSystem(const string& key)
{
    return systems.at(key);
}

void arc::SystemHost::start(int group)
{
    for(SystemMapIterator
            it = systems.begin(),
            ite = systems.end();
        it != ite; ++it)
    {
        if(it->second->group == group)
        {
            it->second->start();
        }
    }
}

void arc::SystemHost::update(int group)
{
    for(SystemMapIterator
            it = systems.begin(),
            ite = systems.end();
        it != ite; ++it)
    {
        if(it->second->group == group)
        {
            it->second->update();
        }
    }
}

void arc::SystemHost::stop(int group)
{
    for(SystemMapIterator
            it = systems.begin(),
            ite = systems.end();
        it != ite; ++it)
    {
        if(it->second->group == group)
        {
            it->second->stop();
        }
    }
}

int arc::SystemHost::createGroup()
{
    return groupCount++;
}