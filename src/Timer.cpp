#include <arc/Timer.h>

void arc::Timer::start()
{
    clock.restart();
    running = true;
}

void arc::Timer::stop()
{
    running = false;
}

bool arc::Timer::isRunning()
{
    return running;
}

int arc::Timer::getTime()
{
    sf::Time result = sf::milliseconds(0);

    if(running)
    {
        result = clock.getElapsedTime();
    }

    return result.asMilliseconds();
}