#include <arc/GraphicEntity.h>

bool arc::GraphicEntity::containsPoint(int x, int y)
{
    sf::Vector2f
        coord = getTransformable().getPosition(),
        farCoord = coord + (sf::Vector2f)getSize();

    return
        x > coord.x &&
        y > coord.y &&
        x < farCoord.x &&
        y < farCoord.y;
}

bool arc::GraphicEntity::containsPoint(const sf::Vector2f& target)
{
    return containsPoint(target.x, target.y);
}

void arc::GraphicEntity::setLeft(float pos)
{
    sf::Transformable* entity = &getTransformable();
    sf::Vector2f origPos = entity->getPosition();
    origPos.x = pos;
    entity->setPosition(origPos);
    usingRight = false;
}

void arc::GraphicEntity::setTop(float pos)
{
    sf::Transformable* entity = &getTransformable();
    sf::Vector2f origPos = entity->getPosition();
    origPos.y = pos;
    entity->setPosition(origPos);
    usingBottom = false;
}

void arc::GraphicEntity::setRight(float pos)
{
    right = pos;
    usingRight = true;
}

void arc::GraphicEntity::setBottom(float pos)
{
    bottom = pos;
    usingBottom = true;
}

float arc::GraphicEntity::getRight()
{
    return right;
}

float arc::GraphicEntity::getBottom()
{
    return bottom;
}

bool arc::GraphicEntity::isUsingRight()
{
    return usingRight;
}

bool arc::GraphicEntity::isUsingBottom()
{
    return usingBottom;
}

bool arc::GraphicEntity::hasSizeChanged(const sf::Vector2i& size)
{
    sf::Vector2i previous = prevSize;
    prevSize = size;
    return previous != size;
}