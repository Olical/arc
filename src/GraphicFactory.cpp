#include <arc/GraphicFactory.h>

shared_ptr<arc::ImageGraphicEntity> arc::GraphicFactory::createImage(
    shared_ptr<ImageAssetEntity> image,
    const sf::Vector2f& position)
{
    shared_ptr<ImageGraphicEntity> entity(
        new ImageGraphicEntity(image));
    entity->getTransformable().setPosition(position);
    addEntity(entity);
    return entity;
}

shared_ptr<arc::SpriteGraphicEntity> arc::GraphicFactory::createSprite(
    shared_ptr<ImageAssetEntity> image,
    const sf::Vector2f& cellSize,
    const sf::Vector2f& position,
    const sf::Vector2i& cell)
{
    shared_ptr<SpriteGraphicEntity> entity(
        new SpriteGraphicEntity(image));
    entity->setCellSize(cellSize);
    entity->getTransformable().setPosition(position);
    entity->setCell(cell);
    addEntity(entity);
    return entity;
}

shared_ptr<arc::TextGraphicEntity> arc::GraphicFactory::createText(
    const string& content,
    shared_ptr<FontAssetEntity> font,
    int size,
    const sf::Color& color,
    const sf::Vector2f& position)
{
    shared_ptr<TextGraphicEntity> entity(new TextGraphicEntity(
        content, font, size, color));
    entity->getTransformable().setPosition(position);
    addEntity(entity);
    return entity;
}