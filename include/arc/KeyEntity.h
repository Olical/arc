#ifndef ARC_KEYENTITY_H
#define ARC_KEYENTITY_H

#include <SFML/Graphics.hpp>

#include <arc/Entity.h>

namespace arc
{
    class KeyEntity : public Entity
    {
    public:
        KeyEntity() : Entity() {}
        bool isPressed(const sf::Keyboard::Key&);
        virtual void handle() = 0;
    };
}

#endif