#ifndef ARC_ENTITY_H
#define ARC_ENTITY_H

#include <boost/shared_ptr.hpp>

#include <arc/SystemHost.h>

using boost::shared_ptr;

namespace arc
{
    class Entity
    {
    public:
        int order, group;
        SystemHost* host;

        Entity() : order(0), group(0), host(0) {}
        virtual ~Entity() {}
    };

    struct EntityOrder
    {
        bool operator()(shared_ptr<Entity> a, shared_ptr<Entity> b) const
        {
            return a->order <= b->order;
        }
    };
}

#endif