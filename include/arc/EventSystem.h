#ifndef ARC_EVENTSYSTEM_H
#define ARC_EVENTSYSTEM_H

#include <string>

#include <SFML/Graphics.hpp>

#include <arc/System.h>
#include <arc/EventEntity.h>
#include <arc/GraphicSystem.h>
#include <arc/SystemHost.h>

using namespace std;

namespace arc
{
    class EventSystem : public System<EventEntity>
    {
        GraphicSystem* graphics;

    public:
        static const string key;
        bool autoClose;

        EventSystem() : graphics(0), autoClose(true) {}
        void start();
        void update();
    };
}

#endif