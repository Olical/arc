#ifndef ARC_ASSETENTITY_H
#define ARC_ASSETENTITY_H

#include <string>

#include <arc/Entity.h>

using namespace std;

namespace arc
{
    class AssetEntity : public Entity
    {
    public:
        string file;

        AssetEntity(const string& file) : Entity(), file(file) {}
        virtual void load() = 0;
    };
}

#endif