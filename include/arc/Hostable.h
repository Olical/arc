#ifndef ARC_HOSTABLE_H
#define ARC_HOSTABLE_H

namespace arc
{
    class SystemHost;

    class Hostable
    {
    public:
        int group;
        SystemHost* host;

        virtual ~Hostable() {}
        virtual void start() {}
        virtual void update() {}
        virtual void stop() {}
    };
}

#endif