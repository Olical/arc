#ifndef ARC_TEXTGRAPHICENTITY_H
#define ARC_TEXTGRAPHICENTITY_H

#include <string>

#include <SFML/Graphics.hpp>
#include <boost/shared_ptr.hpp>

#include <arc/GraphicEntity.h>
#include <arc/FontAssetEntity.h>

using namespace std;
using boost::shared_ptr;

namespace arc
{
    class TextGraphicEntity : public GraphicEntity
    {
        sf::Text text;
        string rawText;
        int width;

    public:
        TextGraphicEntity(
            const string& content,
            shared_ptr<FontAssetEntity> font,
            int size = 16,
            const sf::Color& color = sf::Color::Black) :
                GraphicEntity(),
                width(-1)
        {
            text.setFont(font->font);
            text.setCharacterSize(size);
            setText(content);
            setColor(color);
        }
        sf::Drawable& getDrawable();
        sf::Transformable& getTransformable();
        sf::Vector2i getSize();
        void setText(const string&);
        string getText();
        void setFont(shared_ptr<FontAssetEntity>);
        void setColor(const sf::Color&);
        void setStyle(int);
        void setSize(int);
        sf::Vector2f getCharacterPosition(int);
        void insertLineBreaks();
        void setWidth(int);
    };
}

#endif