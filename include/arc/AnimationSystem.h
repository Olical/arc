#ifndef ARC_ANIMATIONSYSTEM_H
#define ARC_ANIMATIONSYSTEM_H

#include <string>

#include <arc/System.h>
#include <arc/AnimationEntity.h>

using namespace std;

namespace arc
{
    class AnimationSystem : public System<AnimationEntity>
    {
    public:
        static const string key;

        void update();
    };
}

#endif