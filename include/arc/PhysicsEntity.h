#ifndef ARC_PHYSICSENTITY_H
#define ARC_PHYSICSENTITY_H

#include <arc/Entity.h>

namespace arc
{
    class PhysicsEntity : public Entity
    {
    public:
        PhysicsEntity() : Entity() {}
        virtual ~PhysicsEntity() {};
        virtual void update() {};
    };
}

#endif