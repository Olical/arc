#ifndef ARC_EVENTENTITY_H
#define ARC_EVENTENTITY_H

#include <SFML/Graphics.hpp>

#include <arc/Entity.h>

namespace arc
{
    class EventEntity : public Entity
    {
    public:
        EventEntity() : Entity() {}
        virtual void handle(const sf::Event&) = 0;
    };
}

#endif