#ifndef ARC_ENTITYFACTORY_H
#define ARC_ENTITYFACTORY_H

#include <boost/shared_ptr.hpp>

#include <arc/System.h>

using boost::shared_ptr;

namespace arc
{
    template <class T, class S>
    class EntityFactory
    {
    public:
        S* system;

        EntityFactory(S& system) : system(&system) {}
        void addEntity(shared_ptr<T>);
    };

    template <class T, class S>
    void EntityFactory<T, S>::addEntity(shared_ptr<T> entity)
    {
        system->addEntity(entity);
    }
}

#endif