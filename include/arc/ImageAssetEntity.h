#ifndef ARC_IMAGEASSETENTITY_H
#define ARC_IMAGEASSETENTITY_H

#include <string>

#include <SFML/Graphics.hpp>

#include <arc/AssetEntity.h>

using namespace std;

namespace arc
{
    class ImageAssetEntity : public AssetEntity
    {
    public:
        sf::Texture texture;

        ImageAssetEntity(const string& file) : AssetEntity(file) {}
        void load();
    };
}

#endif