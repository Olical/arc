#ifndef ARC_PHYSICSSYSTEM_H
#define ARC_PHYSICSSYSTEM_H

#include <string>

#include <Box2D/Box2D.h>

#include <arc/System.h>
#include <arc/PhysicsEntity.h>

using namespace std;

namespace arc
{
    class PhysicsSystem : public System<PhysicsEntity>
    {
    public:
        static const string key;

        b2World world;
        float timeStep;
        int ppm;
        int velocityIterations, positionIterations;

        PhysicsSystem(
            const b2Vec2& gravity = b2Vec2(0.0f, 10.0f),
            float timeStep = 1.0f / 60.0f,
            int ppm = 40,
            int velocityIterations = 6,
            int positionIterations = 8) :
                world(gravity),
                timeStep(timeStep),
                ppm(ppm),
                velocityIterations(velocityIterations),
                positionIterations(positionIterations) {}
        void update();
    };
}

#endif