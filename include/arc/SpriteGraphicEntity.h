#ifndef ARC_SPITEGRAPHICENTITY_H
#define ARC_SPITEGRAPHICENTITY_H

#include <SFML/Graphics.hpp>
#include <boost/shared_ptr.hpp>

#include <arc/ImageGraphicEntity.h>
#include <arc/ImageAssetEntity.h>

using boost::shared_ptr;

namespace arc
{
    class SpriteGraphicEntity : public ImageGraphicEntity
    {
        sf::Vector2f cellSize;

    public:
        SpriteGraphicEntity(shared_ptr<ImageAssetEntity> image) :
            ImageGraphicEntity(image) {}
        void setCellSize(const sf::Vector2f&);
        void setCell(const sf::Vector2i&);
    };
}

#endif