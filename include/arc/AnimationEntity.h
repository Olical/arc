#ifndef ARC_ANIMATIONENTITY_H
#define ARC_ANIMATIONENTITY_H

#include <string>

#include <arc/Entity.h>
#include <arc/Timer.h>
#include <arc/easing.h>

using namespace std;

namespace arc
{
    class AnimationEntity : public Entity
    {
    protected:
        typedef float (*EasingMethod)(float, float, float, float);
        // If that's what EasingMethods your boat...

    private:
        float value, from, to, change;
        bool loop;
        int duration, lastCalc;
        EasingMethod easing;
        Timer timer;

    public:
        AnimationEntity(EasingMethod easing = easing::linear) :
            Entity(),
            loop(false),
            easing(easing) {}
        void update();
        float getValue();
        void start(float, float, int);
        void stop();
        bool isRunning();
        void setLoop(bool);
    };
}

#endif