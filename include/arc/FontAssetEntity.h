#ifndef ARC_FONTASSETENTITY_H
#define ARC_FONTASSETENTITY_H

#include <string>

#include <SFML/Graphics.hpp>

#include <arc/AssetEntity.h>

using namespace std;

namespace arc
{
    class FontAssetEntity : public AssetEntity
    {
    public:
        sf::Font font;

        FontAssetEntity(const string& file) : AssetEntity(file) {}
        void load();
    };
}

#endif