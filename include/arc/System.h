#ifndef ARC_SYSTEM_H
#define ARC_SYSTEM_H

#include <set>

#include <boost/shared_ptr.hpp>

#include <arc/Hostable.h>
#include <arc/Entity.h>

using namespace std;
using boost::shared_ptr;

namespace arc
{
    template <class T>
    class System : public Hostable
    {
    protected:
        typedef set<shared_ptr<T>, EntityOrder> Entities;
        typedef typename Entities::iterator iterator;
        typedef typename Entities::const_iterator const_iterator;
        Entities entities;

    public:
        void addEntity(shared_ptr<T>);
        void removeEntity(shared_ptr<T>);
        void removeGroup(int);
        void setOrder(shared_ptr<Entity>, int);
        void updateOrder(shared_ptr<Entity>);
    };

    template <class T>
    void System<T>::addEntity(shared_ptr<T> entity)
    {
        entities.insert(entity);

        if(entity->host == 0)
        {
            entity->host = host;
        }
    }

    template <class T>
    void System<T>::removeEntity(shared_ptr<T> entity)
    {
        for(const_iterator
                it = entities.begin(),
                ite = entities.end();
            it != ite; ++it)
        {
            if(entity.get() == (*it).get())
            {
                entities.erase(it);
            }
        }
    }

    template <class T>
    void System<T>::removeGroup(int group)
    {
        for(const_iterator
                it = entities.begin(),
                ite = entities.end();
            it != ite; ++it)
        {
            if((*it)->group == group)
            {
                entities.erase(it);
            }
        }
    }

    template <class T>
    void System<T>::setOrder(shared_ptr<Entity> entity, int order)
    {
        entity->order = order;
        updateOrder();
    }

    template <class T>
    void System<T>::updateOrder(shared_ptr<Entity> entity)
    {
        removeEntity(entity);
        addEntity(entity);
    }
}

#endif