#ifndef ARC_ASSETSYSTEM_H
#define ARC_ASSETSYSTEM_H

#include <string>

#include <boost/shared_ptr.hpp>

#include <arc/System.h>
#include <arc/AssetEntity.h>

using namespace std;
using boost::shared_ptr;

namespace arc
{
    class AssetSystem : public System<AssetEntity>
    {
    public:
        static const string key;

        bool find(const string&);
        shared_ptr<AssetEntity> get(const string&);
    };
}

#endif