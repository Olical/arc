#ifndef ARC_GRAPHICSYSTEM_H
#define ARC_GRAPHICSYSTEM_H

#include <string>
#include <math.h>

#include <SFML/Graphics.hpp>
#include <boost/shared_ptr.hpp>

#include <arc/System.h>
#include <arc/GraphicEntity.h>

using namespace std;
using boost::shared_ptr;

namespace arc
{
    class GraphicSystem : public System<GraphicEntity>
    {
        sf::VideoMode videoMode;
        string title;
        sf::Color clearColor;
        sf::Clock clock;
        sf::ContextSettings settings;
        int limit, style;
        sf::Vector2u prevSize;
        shared_ptr<GraphicEntity> noFocus;

    public:
        static const string key;
        int fps;
        sf::RenderWindow window;

        GraphicSystem(
            const sf::VideoMode& videoMode,
            const string& title,
            const sf::Color& clearColor = sf::Color::Black,
            int style = sf::Style::Default) :
                videoMode(videoMode),
                title(title),
                clearColor(clearColor),
                style(style)
        {
            setFramerateLimit(60);
        }
        void start();
        void update();
        void stop();
        void clearWindow();
        void setFramerateLimit(int);
        shared_ptr<GraphicEntity> entityAtPoint(int, int, bool = false, int = 0);

        shared_ptr<GraphicEntity> getFocussed();
        void setFocussed(shared_ptr<GraphicEntity>);
        void setFocussed(int, int, int = 0);
        float getBearingDistance(
            const sf::Vector2f&, const sf::Vector2f&, float);
        float getDistance(const sf::Vector2f&, const sf::Vector2f&);
        void setFocussed(float, int = 0);
    };
}

#endif