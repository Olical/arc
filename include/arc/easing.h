#ifndef ARC_EASING_H
#define ARC_EASING_H

#include <math.h>

namespace arc
{
    namespace easing
    {
        float linear(float, float, float, float);

        namespace back
        {
            const float s = 1.70158f;
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace bounce
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace circ
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace cubic
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace elastic
        {
            const float a = 0.2f, p = 0.0f;
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace expo
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace quad
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace quart
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace quint
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }

        namespace sine
        {
            float in(float, float, float, float);
            float out(float, float, float, float);
            float inOut(float, float, float, float);
        }
    }
}

#endif