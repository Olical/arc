#ifndef ARC_PERSISTENCESYSTEM_H
#define ARC_PERSISTENCESYSTEM_H

#include <string>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <arc/System.h>
#include <arc/PersistenceEntity.h>

using namespace std;
using boost::property_tree::ptree;

namespace arc
{
    class PersistenceSystem : public System<PersistenceEntity>
    {
        string file;

    public:
        static const string key;

        PersistenceSystem(const string& file) : file(file) {}
        void start();
        void stop();
    };
}

#endif