#ifndef ARC_ASSETFACTORY_H
#define ARC_ASSETFACTORY_H

#include <boost/shared_ptr.hpp>

#include <arc/EntityFactory.h>
#include <arc/AssetSystem.h>
#include <arc/AssetEntity.h>

#include <arc/ImageAssetEntity.h>
#include <arc/SoundAssetEntity.h>
#include <arc/MusicAssetEntity.h>
#include <arc/XMLAssetEntity.h>
#include <arc/I18nAssetEntity.h>
#include <arc/FontAssetEntity.h>

using boost::shared_ptr;
using boost::static_pointer_cast;

namespace arc
{
    class AssetFactory : public EntityFactory<AssetEntity, AssetSystem>
    {
    public:
        template<class T>
        shared_ptr<T> get(const string& file)
        {
            if(system->find(file))
            {
                return static_pointer_cast<T>(system->get(file));
            }

            shared_ptr<T> asset(new T(file));
            asset->load();
            addEntity(asset);
            return asset;
        }

        AssetFactory(AssetSystem& system) : EntityFactory(system) {}
        shared_ptr<ImageAssetEntity> getImage(const string&);
        shared_ptr<SoundAssetEntity> getSound(const string&);
        shared_ptr<MusicAssetEntity> getMusic(const string&);
        shared_ptr<XMLAssetEntity> getXML(const string&);
        shared_ptr<I18nAssetEntity> getI18n(const string&, const string&);
        shared_ptr<FontAssetEntity> getFont(const string&);
    };
}

#endif