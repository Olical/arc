#ifndef ARC_XMLASSETENTITY_H
#define ARC_XMLASSETENTITY_H

#include <string>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <arc/AssetEntity.h>

using namespace std;
using boost::property_tree::ptree;

namespace arc
{
    class XMLAssetEntity : public AssetEntity
    {
    public:
        ptree tree;

        XMLAssetEntity(const string& file) : AssetEntity(file) {}
        void load();
    };
}

#endif