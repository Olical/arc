#ifndef ARC_TIMER_H
#define ARC_TIMER_H

#include <SFML/Graphics.hpp>

namespace arc
{
    class Timer
    {
        sf::Clock clock;
        bool running;

    public:
        Timer() : running(false) {}
        void start();
        void pause();
        void stop();
        bool isRunning();
        int getTime();
    };
}

#endif