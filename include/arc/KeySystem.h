#ifndef ARC_KEYSYSTEM_H
#define ARC_KEYSYSTEM_H

#include <string>

#include <SFML/Graphics.hpp>

#include <arc/System.h>
#include <arc/KeyEntity.h>

using namespace std;

namespace arc
{
    class KeySystem : public System<KeyEntity>
    {
    public:
        static const string key;

        void update();
    };
}

#endif