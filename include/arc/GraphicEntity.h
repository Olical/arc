#ifndef ARC_GRAPHICENTITY_H
#define ARC_GRAPHICENTITY_H

#include <SFML/Graphics.hpp>

#include <arc/Entity.h>

namespace arc
{
    class GraphicEntity : public Entity
    {
        bool usingRight, usingBottom;
        float right, bottom;
        sf::Vector2i prevSize;

    public:
        int focusGroup;
        bool focussed, focusable;

        GraphicEntity() :
            Entity(),
            usingRight(false),
            usingBottom(false),
            focusGroup(0),
            focussed(false),
            focusable(false) {}
        virtual sf::Drawable& getDrawable() = 0;
        virtual sf::Transformable& getTransformable() = 0;
        virtual sf::Vector2i getSize() = 0;

        virtual void applyFocus() {}
        virtual void removeFocus() {}
        bool containsPoint(int, int);
        bool containsPoint(const sf::Vector2f&);

        void setLeft(float);
        void setTop(float);

        void setRight(float);
        void setBottom(float);
        float getRight();
        float getBottom();
        bool isUsingRight();
        bool isUsingBottom();
        bool hasSizeChanged(const sf::Vector2i&);
    };
}

#endif