#ifndef ARC_UTILS_H
#define ARC_UTILS_H

#include <string>

#include <SFML/Graphics.hpp>
#include <boost/filesystem.hpp>

using namespace std;

namespace fs = boost::filesystem;

namespace arc
{
    namespace utils
    {
        string relPath(const string&, const string&);
        sf::Vector3f relSound(const sf::Vector2f&, const sf::Vector2u&);
    };
}

#endif