#ifndef ARC_GRAPHICPHYSICSENTITY_H
#define ARC_GRAPHICPHYSICSENTITY_H

#include <boost/shared_ptr.hpp>

#include <arc/Entity.h>
#include <arc/PhysicsEntity.h>
#include <arc/GraphicEntity.h>

using boost::shared_ptr;

namespace arc
{
    class GraphicPhysicsEntity : public PhysicsEntity
    {
    public:
        shared_ptr<GraphicEntity> graphics;

        GraphicPhysicsEntity() : PhysicsEntity() {}
        virtual void update() = 0;
    };
}

#endif