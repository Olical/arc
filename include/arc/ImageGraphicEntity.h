#ifndef ARC_IMAGEGRAPHICENTITY_H
#define ARC_IMAGEGRAPHICENTITY_H

#include <SFML/Graphics.hpp>
#include <boost/shared_ptr.hpp>

#include <arc/GraphicEntity.h>
#include <arc/ImageAssetEntity.h>

using boost::shared_ptr;

namespace arc
{
    class ImageGraphicEntity : public GraphicEntity
    {
    protected:
        sf::Sprite sprite;
        sf::Vector2i size;

    public:
        ImageGraphicEntity(
            shared_ptr<ImageAssetEntity> image) :
                GraphicEntity(),
                sprite(image->texture),
                size(image->texture.getSize()) {}
        sf::Drawable& getDrawable();
        sf::Transformable& getTransformable();
        sf::Vector2i getSize();
    };
}

#endif