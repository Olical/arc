#ifndef ARC_GRAPHICFACTORY_H
#define ARC_GRAPHICFACTORY_H

#include <string>

#include <boost/shared_ptr.hpp>

#include <arc/EntityFactory.h>
#include <arc/GraphicSystem.h>
#include <arc/GraphicEntity.h>

#include <arc/ImageGraphicEntity.h>
#include <arc/SpriteGraphicEntity.h>
#include <arc/TextGraphicEntity.h>

#include <arc/ImageAssetEntity.h>
#include <arc/FontAssetEntity.h>

using namespace std;
using boost::shared_ptr;

namespace arc
{
    class GraphicFactory : public EntityFactory<GraphicEntity, GraphicSystem>
    {
    public:
        GraphicFactory(GraphicSystem& system) : EntityFactory(system) {}
        shared_ptr<ImageGraphicEntity> createImage(
            shared_ptr<ImageAssetEntity>,
            const sf::Vector2f& = sf::Vector2f(0, 0));
        shared_ptr<SpriteGraphicEntity> createSprite(
            shared_ptr<ImageAssetEntity>,
            const sf::Vector2f&,
            const sf::Vector2f& = sf::Vector2f(0, 0),
            const sf::Vector2i& = sf::Vector2i(0, 0));
        shared_ptr<TextGraphicEntity> createText(
            const string&,
            shared_ptr<FontAssetEntity>,
            int = 16,
            const sf::Color& color = sf::Color::Black,
            const sf::Vector2f& = sf::Vector2f(0, 0));
    };
}

#endif