#ifndef ARC_PERSISTENCEENTITY_H
#define ARC_PERSISTENCEENTITY_H

#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

#include <arc/Entity.h>

using namespace std;
using boost::property_tree::ptree;

namespace fs = boost::filesystem;

namespace arc
{
    class PersistenceEntity : public Entity
    {
    public:
        PersistenceEntity() : Entity() {}
        virtual void load(const ptree&) = 0;
        virtual void save(ptree&) = 0;
    };
}

#endif