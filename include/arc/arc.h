#ifndef ARC_H
#define ARC_H

#include <arc/Hostable.h>
#include <arc/Timer.h>
#include <arc/utils.h>

#include <arc/SystemHost.h>
#include <arc/System.h>
#include <arc/Entity.h>
#include <arc/EntityFactory.h>

#include <arc/GraphicSystem.h>
#include <arc/GraphicEntity.h>
#include <arc/ImageGraphicEntity.h>
#include <arc/SpriteGraphicEntity.h>
#include <arc/TextGraphicEntity.h>
#include <arc/GraphicFactory.h>

#include <arc/PhysicsSystem.h>
#include <arc/PhysicsEntity.h>
#include <arc/GraphicPhysicsEntity.h>

#include <arc/EventSystem.h>
#include <arc/EventEntity.h>

#include <arc/KeySystem.h>
#include <arc/KeyEntity.h>

#include <arc/PersistenceSystem.h>
#include <arc/PersistenceEntity.h>

#include <arc/AnimationSystem.h>
#include <arc/AnimationEntity.h>
#include <arc/easing.h>

#include <arc/AssetSystem.h>
#include <arc/AssetEntity.h>
#include <arc/ImageAssetEntity.h>
#include <arc/SoundAssetEntity.h>
#include <arc/MusicAssetEntity.h>
#include <arc/XMLAssetEntity.h>
#include <arc/I18nAssetEntity.h>
#include <arc/FontAssetEntity.h>
#include <arc/AssetFactory.h>

#endif