#ifndef ARC_MUSICASSETENTITY_H
#define ARC_MUSICASSETENTITY_H

#include <string>

#include <SFML/Audio.hpp>

#include <arc/AssetEntity.h>

using namespace std;

namespace arc
{
    class MusicAssetEntity : public AssetEntity
    {
    public:
        sf::Music music;

        MusicAssetEntity(const string& file) : AssetEntity(file) {}
        void load();
    };
}

#endif