#ifndef ARC_SOUNDASSETENTITY_H
#define ARC_SOUNDASSETENTITY_H

#include <string>

#include <SFML/Audio.hpp>

#include <arc/AssetEntity.h>

using namespace std;

namespace arc
{
    class SoundAssetEntity : public AssetEntity
    {
    public:
        sf::SoundBuffer buffer;
        sf::Sound sound;

        SoundAssetEntity(const string& file) : AssetEntity(file) {}
        void load();
    };
}

#endif