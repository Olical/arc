#ifndef ARC_SYSTEMHOST_H
#define ARC_SYSTEMHOST_H

#include <string>
#include <map>

#include <arc/Hostable.h>

using namespace std;

namespace arc
{
    typedef map<const string, Hostable*> SystemMap;
    typedef SystemMap::const_iterator SystemMapIterator;
    typedef pair<const string, Hostable*> SystemPair;

    class SystemHost
    {
        SystemMap systems;
        int groupCount;

    public:
        SystemHost() : groupCount(0) {}
        void addSystem(const string&, Hostable&, int = 0);
        void removeSystem(const string&);
        Hostable* getSystem(const string&);
        void start(int = 0);
        void update(int = 0);
        void stop(int = 0);
        int createGroup();
    };
}

#endif