#ifndef ARC_I18NASSETENTITY_H
#define ARC_I18NASSETENTITY_H

#include <string>

#include <boost/property_tree/ptree.hpp>

#include <arc/XMLAssetEntity.h>

using namespace std;
using boost::property_tree::ptree;

namespace arc
{
    class I18nAssetEntity : public XMLAssetEntity
    {
    public:
        I18nAssetEntity(const string& file, const string& region) :
            XMLAssetEntity(I18nAssetEntity::buildPath(file, region)) {}
        I18nAssetEntity(const string& file) : XMLAssetEntity(file) {}
        static string buildPath(const string&, const string&);
        string get(const string&, const string& = "i18n");
    };
}

#endif